@extends('layouts.app')
@section('content')
<!DOCTYPE html>
<html>
<head>
	<title>Bugs</title>
</head>
<body>
	
	<div class="container d-flex align-items-center justify-content-center flex-column vh-50 w-100">
		<h1 class="text-align-center py-5">My Bugs</h1>
		<div class="row d-flex align-items-center justify-content-start w-100">
			@foreach($bugs as $indiv_bug)
			<div class="col-lg-4 my-2">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">{{$indiv_bug->title}}</h4>
						<p class="card-text">{{$indiv_bug->body}}</p>
						<p class="card-text">{{$indiv_bug->category->name}}</p>
						<p class="card-text">{{$indiv_bug->status->name}}</p>
						<p class="card-text">{{$indiv_bug->user->name}}</p>
					</div>
					<div class="card-footer d-flex">
						<form action="/deletebug/{{$indiv_bug->id}}" method="POST" >
							@csrf
							@method('DELETE')
							<button class="btn btn-danger" type="subtmit">Delete</button>
						</form>
						<a href="/editbug/{{$indiv_bug->id}}" class="btn btn-success">Edit</a>
						<a href="/indivbug/{{$indiv_bug->id}}" class="btn btn-info">View Details</a>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</body>
</html>
@endsection