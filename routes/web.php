<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dailybuglanding');
});

Auth::routes();



Route::get('/home', 'HomeController@index')->name('home');
//For All
Route::get('/indivbug/{id}', 'BugController@showIndivBug');
//To showBugsData
Route::get('/allbugs', 'BugController@showBugs');
//To show add bug form
Route::get('/addbug', 'BugController@create');
//To save add bug form
Route::post('/addbug', 'BugController@store');
//To show user bugs
Route::get('/mybugs', 'BugController@indivBugs');
//To delete
Route::delete('/deletebug/{id}', 'BugController@destroy');
//To go to edit form
Route::get('/editbug/{id}', 'BugController@edit');
//TO save edited bug
Route::patch('/editbug/{id}', 'BugController@update');
// To accept solution
Route::patch('/accept/{id}', 'BugController@accept');

//Admin Routes
//To go to solve form
Route::get('/solve/{id}','BugController@showSolve');
//To save
Route::post('/solve', 'BugController@saveSolution');
//
Route::delete('/deleteSolution/{id}', 'SolutionController@deleteSolution');

